#!/bin/bash

# Take a argument and test all files with this occurence
# Ex: ./run_test_sh.sh error
for map in `find map -mindepth 1 -name "*$1*"`
do
    echo "+-+-+ Map $map +-+-+"
    ./fillit  $map
done
